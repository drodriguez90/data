/*
 Navicat Premium Data Transfer

 Source Server         : docker_local_mysql
 Source Server Type    : MySQL
 Source Server Version : 80300 (8.3.0)
 Source Host           : localhost:3306
 Source Schema         : devops_questions

 Target Server Type    : MySQL
 Target Server Version : 80300 (8.3.0)
 File Encoding         : 65001

 Date: 02/05/2024 20:18:55
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for questions
-- ----------------------------
DROP TABLE IF EXISTS `questions`;
CREATE TABLE `questions` (
  `id` int NOT NULL,
  `question` text NOT NULL,
  `pregunta` text NOT NULL,
  `category` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of questions
-- ----------------------------
BEGIN;
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (1, 'What are the popular DevOps tools that you use?', '¿Cuáles son las herramientas DevOps más populares que utiliza?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (2, 'What are the main benefits of DevOps?', '¿Cuáles son los principales beneficios de DevOps?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (3, 'What is the typical DevOps workflow you use in your organization?', '¿Cuál es el flujo de trabajo típico de DevOps que utiliza en su organización?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (4, 'How do you take a DevOps approach with Amazon Web Services?', '¿Cómo se adopta un enfoque DevOps con Amazon Web Services?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (5, 'How will you run a script automatically when a developer commits a change into GIT?', '¿Cómo ejecutará un script automáticamente cuando un desarrollador realice un cambio en GIT?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (6, 'What are the main features of AWS OpsWorks Stacks?', '¿Cuáles son las características principales de AWS OpsWorks Stacks?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (7, 'How does CloudFormation work in AWS?', '¿Cómo funciona CloudFormation en AWS?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (8, 'What is CI/CD in DevOps?', '¿Qué es CI/CD en DevOps?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (9, 'What are the best practices of Continuous Integration (CI)?', '¿Cuáles son las mejores prácticas de Integración Continua (CI)?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (10, 'What are the benefits of Continuous Integration (CI)?', '¿Cuáles son los beneficios de la Integración Continua (CI)?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (11, 'What are the options for security in Jenkins?', '¿Cuáles son las opciones de seguridad en Jenkins?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (12, 'What are the main benefits of Chef?', '¿Cuáles son los principales beneficios de Chef?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (13, 'What is the architecture of Chef?', '¿Cuál es la arquitectura de Chef?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (14, 'What is a Recipe in Chef?', '¿Qué es una receta en Chef?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (15, 'What are the main benefits of Ansible?', '¿Cuáles son los principales beneficios de Ansible?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (16, 'What are the main use cases of Ansible?', '¿Cuáles son los principales casos de uso de Ansible?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (17, 'What is Docker Hub?', '¿Qué es Docker Hub?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (18, 'What is your favorite scripting language for DevOps?', '¿Cuál es tu lenguaje de programación favorito para DevOps?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (19, 'What is Multi-factor authentication?', '¿Qué es la autenticación multifactor?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (20, 'What are the main benefits of Nagios?', '¿Cuáles son los principales beneficios de Nagios?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (21, 'What is State Stalking in Nagios?', '¿Qué es el acoso estatal en Nagios?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (22, 'What are the main features of Nagios?', '¿Cuáles son las principales características de Nagios?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (23, 'What is Puppet?', '¿Qué es la marioneta?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (24, 'What is the architecture of Puppet?', '¿Cuál es la arquitectura de Puppet?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (25, 'What are the main use cases of Puppet Enterprise?', '¿Cuáles son los principales casos de uso de Puppet Enterprise?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (26, 'What is the use of Kubernetes?', '¿Para qué sirve Kubernetes?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (27, 'What is the architecture of Kubernetes?', '¿Cuál es la arquitectura de Kubernetes?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (28, 'How does Kubernetes provide high availability of applications in a Cluster?', '¿Cómo proporciona Kubernetes alta disponibilidad de aplicaciones en un clúster?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (29, 'Why is Automated Testing a must requirement for DevOps?', '¿Por qué las pruebas automatizadas son un requisito imprescindible para DevOps?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (30, 'What is Chaos Monkey in DevOps?', '¿Qué es Chaos Monkey en DevOps?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (31, 'How do you perform Test Automation in DevOps?', '¿Cómo se realiza la automatización de pruebas en DevOps?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (32, 'What are the main services of AWS that you have used?', '¿Cuáles son los principales servicios de AWS que ha utilizado?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (33, 'Why is GIT considered better than CVS for version control system?', '¿Por qué se considera que GIT es mejor que CVS para el sistema de control de versiones?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (34, 'What is the difference between a Container and a Virtual Machine?', '¿Cuál es la diferencia entre un Contenedor y una Máquina Virtual?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (35, 'What is Serverless architecture?', '¿Qué es la arquitectura sin servidor?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (36, 'What are the main principles of DevOps?', '¿Cuáles son los principios fundamentales de DevOps?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (37, 'Are you more Dev or more Ops?', '¿Eres más Dev o más Ops?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (38, 'What is a REST service?', '¿Qué es un servicio REST?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (39, 'What are the Three Ways of DevOps?', '¿Cuáles son las tres formas de DevOps?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (40, 'How do you apply DevOps principles to make a system secure?', '¿Cómo se aplican los principios de DevOps para hacer que un sistema sea seguro?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (41, 'What is Self-testing Code?', '¿Qué es el código de autoprueba?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (42, 'What is a Deployment Pipeline?', '¿Qué es un canal de implementación?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (43, 'What are the main features of Docker Hub?', '¿Cuáles son las características principales de Docker Hub?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (44, 'What are the security benefits of using Container-based systems?', '¿Cuáles son los beneficios de seguridad del uso de sistemas basados ​​en contenedores?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (45, 'How many heads can you create in a GIT repository?', '¿Cuántas cabezas puedes crear en un repositorio GIT?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (46, 'What is a Passive check in Nagios?', '¿Qué es un control pasivo en Nagios?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (47, 'What is a Docker container?', '¿Qué es un contenedor Docker?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (48, 'How will you remove an image from Docker?', '¿Cómo eliminarás una imagen de Docker?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (49, 'What are the common use cases of Docker?', '¿Cuáles son los casos de uso comunes de Docker?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (50, 'Can we lose our data when a Docker Container exits?', '¿Podemos perder nuestros datos cuando sale un Docker Container?', 'DevOps');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (51, 'What is Docker?', '¿Qué es Docker?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (52, 'What is the difference between Docker image and Docker container?', '¿Cuál es la diferencia entre la imagen de Docker y el contenedor de Docker?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (53, 'How is a Docker container different from a hypervisor?', '¿En qué se diferencia un contenedor Docker de un hipervisor?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (54, 'Can we write a compose file in JSON file instead of YAML?', '¿Podemos escribir un archivo de redacción en un archivo JSON en lugar de YAML?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (55, 'Can we run multiple apps on one server with Docker?', '¿Podemos ejecutar varias aplicaciones en un servidor con Docker?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (56, 'What are the main features of Docker-compose?', '¿Cuáles son las características principales de Docker-compose?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (57, 'What is the most popular use of Docker?', '¿Cuál es el uso más popular de Docker?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (58, 'What is the role of open-source development in the popularity of Docker?', '¿Cuál es el papel del desarrollo de código abierto en la popularidad de Docker?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (59, 'What is the difference between Docker commands: up, run, and start?', '¿Cuál es la diferencia entre los comandos de Docker: arriba, ejecutar e iniciar?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (60, 'What is Docker Swarm?', '¿Qué es Docker Swarm?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (61, 'What are the features of Docker Swarm?', '¿Cuáles son las características de Docker Swarm?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (62, 'What is a Docker Image?', '¿Qué es una imagen de Docker?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (63, 'What is a Docker Container?', '¿Qué es un contenedor Docker?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (64, 'What is Docker Machine?', '¿Qué es la máquina Docker?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (65, 'Why do we use Docker Machine?', '¿Por qué utilizamos Docker Machine?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (66, 'How will you create a Container in Docker?', '¿Cómo crearás un contenedor en Docker?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (67, 'Do you think Docker is Application-centric or Machine-centric?', '¿Crees que Docker está centrado en las aplicaciones o en las máquinas?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (68, 'Can we run more than one process in a Docker container?', '¿Podemos ejecutar más de un proceso en un contenedor Docker?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (69, 'What are the objects created by Docker Cloud in Amazon Web Services (AWS) EC2?', '¿Cuáles son los objetos creados por Docker Cloud en Amazon Web Services (AWS) EC2?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (70, 'How will you take backup of Docker container volumes in AWS S3?', '¿Cómo realizará una copia de seguridad de los volúmenes de contenedores de Docker en AWS S3?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (71, 'What are the three main steps of Docker Compose?', '¿Cuáles son los tres pasos principales de Docker Compose?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (72, 'What is Pluggable Storage Driver architecture in Docker-based containers?', '¿Qué es la arquitectura del controlador de almacenamiento conectable en contenedores basados ​​en Docker?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (73, 'What are the main security concerns with Docker-based containers?', '¿Cuáles son las principales preocupaciones de seguridad con los contenedores basados ​​en Docker?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (74, 'How can we check the status of a Container in Docker?', '¿Cómo podemos comprobar el estado de un Contenedor en Docker?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (75, 'What are the main benefits of using Docker?', '¿Cuáles son los principales beneficios de utilizar Docker?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (76, 'How does Docker simplify the Software Development process?', '¿Cómo simplifica Docker el proceso de desarrollo de software?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (77, 'What is the basic architecture behind Docker?', '¿Cuál es la arquitectura básica detrás de Docker?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (78, 'What are the popular tasks that you can do with Docker Command-line tool?', '¿Cuáles son las tareas populares que puede realizar con la herramienta de línea de comandos Docker?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (79, 'What type of applications - Stateless or Stateful are more suitable for Docker Container?', '¿Qué tipo de aplicaciones, sin estado o con estado, son más adecuadas para Docker Container?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (80, 'How can Docker run on different Linux distributions?', '¿Cómo se puede ejecutar Docker en diferentes distribuciones de Linux?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (81, 'Why do we use Docker on top of a virtual machine?', '¿Por qué usamos Docker encima de una máquina virtual?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (82, 'How can Docker containers share resources?', '¿Cómo pueden los contenedores Docker compartir recursos?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (83, 'What is the difference between Add and Copy command in a Dockerfile?', '¿Cuál es la diferencia entre el comando Agregar y Copiar en un Dockerfile?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (84, 'What is Docker Entrypoint?', '¿Qué es el punto de entrada de Docker?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (85, 'What is ONBUILD command in Docker?', '¿Qué es el comando ONBUILD en Docker?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (86, 'What is Build cache in Docker?', '¿Qué es la caché de compilación en Docker?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (87, 'What are the most common instructions in a Dockerfile?', '¿Cuáles son las instrucciones más comunes en un Dockerfile?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (88, 'What is the purpose of the EXPOSE command in a Dockerfile?', '¿Cuál es el propósito del comando EXPOSE en un Dockerfile?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (89, 'What are the different kinds of namespaces available in a Container?', '¿Cuáles son los diferentes tipos de espacios de nombres disponibles en un contenedor?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (90, 'How will you monitor Docker in production?', '¿Cómo monitoreará Docker en producción?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (91, 'What are the Cloud platforms that support Docker?', '¿Cuáles son las plataformas en la nube que admiten Docker?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (92, 'How can we control the startup order of services in Docker compose?', '¿Cómo podemos controlar el orden de inicio de los servicios en Docker Compose?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (93, 'Why Docker compose does not wait for a container to be ready before moving onto start the next service independently order?', '¿Por qué Docker Compose no espera a que un contenedor esté listo antes de iniciar el siguiente pedido de servicio de forma independiente?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (94, 'How will you customize Docker compose file for different environments?', '¿Cómo personalizará el archivo de redacción de Docker para diferentes entornos?', 'Docker');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (95, 'What are the benefits of Cloud Computing?', '¿Cuáles son los beneficios de la computación en la nube?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (96, 'What is On-demand computing in Cloud Computing?', '¿Qué es la informática bajo demanda en el Cloud Computing?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (97, 'What are the different layers of Cloud computing?', '¿Cuáles son las diferentes capas de la computación en la nube?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (98, 'What resources are provided by Infrastructure as a Service (IAAS) provider?', '¿Qué recursos proporciona el proveedor de infraestructura como servicio (IAAS)?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (99, 'What is the benefit of Platform as a Service?', '¿Cuál es el beneficio de la plataforma como servicio?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (100, 'What are the main advantages of PaaS?', '¿Cuáles son las principales ventajas de PaaS?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (101, 'What is the main disadvantage of PaaS?', '¿Cuál es la principal desventaja de PaaS?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (102, 'What are the different deployment models in cloud computing?', '¿Cuáles son los diferentes modelos de implementación en la computación en la nube?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (103, 'What is the difference between Scalability and Elasticity?', '¿Cuál es la diferencia entre escalabilidad y elasticidad?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (104, 'What is Software as a Service?', '¿Qué es el software como servicio?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (105, 'What are the different types of Datacenters in cloud computing?', '¿Cuáles son los diferentes tipos de Centros de Datos en la computación en la nube?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (106, 'Explain the various modes of Software as a Service (SaaS) cloud environment?', '¿Explique los distintos modos de entorno de nube de software como servicio (SaaS)?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (107, 'What are the important things to care about in Security in a cloud environment?', '¿Cuáles son los aspectos importantes a tener en cuenta en la seguridad en un entorno de nube?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (108, 'Why do we use API in cloud computing environment?', '¿Por qué utilizamos API en un entorno de computación en la nube?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (109, 'What are the different areas of Security Management in cloud?', '¿Cuáles son las diferentes áreas de la Gestión de la Seguridad en la nube?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (110, 'What are the main cost factors of cloud-based data center?', '¿Cuáles son los principales factores de costo del centro de datos basado en la nube?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (111, 'How can we measure the cloud-based services?', '¿Cómo podemos medir los servicios basados ​​en la nube?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (112, 'How a traditional datacenter is different from a cloud environment?', '¿En qué se diferencia un centro de datos tradicional de un entorno de nube?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (113, 'How will you optimize availability of your application in a Cloud environment?', '¿Cómo optimizará la disponibilidad de su aplicación en un entorno de nube?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (114, 'What are the requirements for implementing IaaS strategy in Cloud?', '¿Cuáles son los requisitos para implementar la estrategia IaaS en la Nube?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (115, 'What is the scenario in which public cloud is preferred over private cloud?', '¿Cuál es el escenario en el que se prefiere la nube pública a la nube privada?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (116, 'Do you think Cloud Computing is a software application or a hardware service?', '¿Crees que Cloud Computing es una aplicación de software o un servicio de hardware?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (117, 'Why companies now prefer Cloud Computing architecture over Client-Server Architecture?', '¿Por qué las empresas prefieren ahora la arquitectura Cloud Computing a la arquitectura Cliente-Servidor?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (118, 'What are the main characteristics of Cloud Computing architecture?', '¿Cuáles son las principales características de la arquitectura Cloud Computing?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (119, 'How databases in Cloud computing are different from traditional databases?', '¿En qué se diferencian las bases de datos en la computación en la nube de las bases de datos tradicionales?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (120, 'What is Virtual Private Network (VPN)?', '¿Qué es la red privada virtual (VPN)?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (121, 'What are the main components of a VPN?', '¿Cuáles son los componentes principales de una VPN?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (122, 'How will you secure the application data for transport in a cloud environment?', '¿Cómo protegerá los datos de la aplicación para su transporte en un entorno de nube?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (123, 'What are the large-scale databases available in Cloud?', '¿Cuáles son las bases de datos a gran escala disponibles en la nube?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (124, 'What are the options for open-source NoSQL database in a Cloud environment?', '¿Cuáles son las opciones para la base de datos NoSQL de código abierto en un entorno de nube?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (125, 'What are the important points to consider before selecting cloud computing?', '¿Cuáles son los puntos importantes a considerar antes de seleccionar la computación en la nube?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (126, 'What is a System integrator in Cloud computing?', '¿Qué es un integrador de sistemas en la computación en la nube?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (127, 'What is virtualization in cloud computing?', '¿Qué es la virtualización en la computación en la nube?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (128, 'What is Eucalyptus in a cloud environment?', '¿Qué es Eucalyptus en un entorno de nube?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (129, 'What are the main components of Eucalyptus cloud architecture?', '¿Cuáles son los componentes principales de la arquitectura de la nube de Eucalyptus?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (130, 'What is Auto-scaling in Cloud computing?', '¿Qué es el escalado automático en la computación en la nube?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (131, 'What are the benefits of Utility Computing model?', '¿Cuáles son los beneficios del modelo de Computación de Utilidades?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (132, 'What is a Hypervisor in Cloud Computing?', '¿Qué es un hipervisor en la computación en la nube?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (133, 'What are the different types of Hypervisor in Cloud Computing?', '¿Cuáles son los diferentes tipos de hipervisor en Cloud Computing?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (134, 'Why Type-1 Hypervisor has better performance than Type-2 Hypervisor?', '¿Por qué el hipervisor tipo 1 tiene mejor rendimiento que el hipervisor tipo 2?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (135, 'What is CaaS?', '¿Qué es CaaS?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (136, 'How is Cloud computing different from computing for mobile devices?', '¿En qué se diferencia la computación en la nube de la computación para dispositivos móviles?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (137, 'Why automation of deployment is very important in Cloud architecture?', '¿Por qué la automatización de la implementación es tan importante en la arquitectura de la nube?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (138, 'What are the main components in Amazon Cloud?', '¿Cuáles son los componentes principales de Amazon Cloud?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (139, 'What are main components in Google Cloud?', '¿Cuáles son los componentes principales de Google Cloud?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (140, 'What are the major offerings of Microsoft Azure Cloud?', '¿Cuáles son las principales ofertas de Microsoft Azure Cloud?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (141, 'What are the reasons of popularity of Cloud Computing architecture?', '¿Cuáles son las razones de la popularidad de la arquitectura de Cloud Computing?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (142, 'What are the Machine Learning options from Google Cloud?', '¿Cuáles son las opciones de Machine Learning de Google Cloud?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (143, 'How will you optimize the Cloud Computing environment?', '¿Cómo optimizará el entorno de Cloud Computing?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (144, 'Do you think Regulations and Legal Compliance is an important aspect of Cloud Computing?', '¿Crees que la Normativa y el Cumplimiento Legal es un aspecto importante del Cloud Computing?', 'CloudComputing');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (145, 'How will you remove all files in current directory? Including the files that are two levels down in a sub-directory', '¿Cómo eliminará todos los archivos del directorio actual? Incluir los archivos que están dos niveles más abajo en un subdirectorio', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (146, 'What is the difference between the -v and -x options in Bash shell scripts?', '¿Cuál es la diferencia entre las opciones -v y -x en los scripts de shell Bash?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (147, 'What is a Filter in Unix command?', '¿Qué es un filtro en el comando Unix?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (148, 'What is Kernel in Unix operating system?', '¿Qué es el Kernel en el sistema operativo Unix?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (149, 'What is a Shell in Unix OS?', '¿Qué es un Shell en el sistema operativo Unix?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (150, 'What are the different shells in Unix that you know about?', '¿Cuáles son los diferentes shells en Unix que conoces?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (151, 'What is the first character of the output in ls -l command?', '¿Cuál es el primer carácter de la salida del comando ls -l?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (152, 'What is the difference between Multi-tasking and Multi-user environment?', '¿Cuál es la diferencia entre entorno multitarea y multiusuario?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (153, 'What is an Inode in Unix?', '¿Qué es un inodo en Unix?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (154, 'What is the difference between absolute path and relative path in Unix file system?', '¿Cuál es la diferencia entre ruta absoluta y ruta relativa en el sistema de archivos Unix?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (155, 'What are the main responsibilities of a Unix Shell?', '¿Cuáles son las principales responsabilidades de un Unix Shell?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (156, 'What is a Shell variable?', '¿Qué es una variable de Shell?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (157, 'What are the important Shell variables that are initialized on starting a Shell?', '¿Cuáles son las variables importantes de Shell que se inicializan al iniciar un Shell?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (158, 'How will you set the value of Environment variables in Unix?', '¿Cómo establecerá el valor de las variables de entorno en Unix?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (159, 'What is the difference between a System Call and a library function?', '¿Cuál es la diferencia entre una llamada al sistema y una función de biblioteca?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (160, 'What are the networking commands in Unix that you have used?', '¿Cuáles son los comandos de red en Unix que ha utilizado?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (161, 'What is a Pipeline in Unix?', '¿Qué es una tubería en Unix?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (162, 'What is the use of tee command in Unix?', '¿Para qué sirve el comando tee en Unix?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (163, 'How will you count the number of lines and words in a file in Unix?', '¿Cómo contarás el número de líneas y palabras de un archivo en Unix?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (164, 'What is Bash shell?', '¿Qué es el shell Bash?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (165, 'How will you search for a name in Unix files?', '¿Cómo buscarás un nombre en archivos Unix?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (166, 'What are the popular options of grep command in Unix?', '¿Cuáles son las opciones populares del comando grep en Unix?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (167, 'What is the difference between whoami and who am i commands in Unix?', '¿Cuál es la diferencia entre los comandos whoami y who am i en Unix?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (168, 'What is a Superuser in Unix?', '¿Qué es un superusuario en Unix?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (169, 'How will you check the information about a process in Unix?', '¿Cómo comprobarás la información sobre un proceso en Unix?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (170, 'What is the use of more command with cat command?', '¿De qué sirve el comando more con el comando cat?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (171, 'What are the File modes in Unix?', '¿Cuáles son los modos de archivo en Unix?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (172, 'We wrote a shell script in Unix but it is not doing anything. What could be the reason?', 'Escribimos un script de shell en Unix pero no hace nada. ¿Cuál podría ser la razón?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (173, 'Question 404 173', 'Pregunta 404 173', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (174, 'Question 404 174', 'Pregunta 404 174', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (175, 'Question 404 175', 'Pregunta 404 175', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (176, 'Question 404 176', 'Pregunta 404 176', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (177, 'What is the significance of 755 in chmod 755 command?', '¿Cuál es el significado de 755 en el comando chmod 755?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (178, 'How can we run a process in background in Unix? How can we kill a process running in background?', '¿Cómo podemos ejecutar un proceso en segundo plano en Unix? ¿Cómo podemos eliminar un proceso que se ejecuta en segundo plano?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (179, 'How will you create a readonly file in Unix?', '¿Cómo crearás un archivo de solo lectura en Unix?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (180, 'How does alias work in Unix?', '¿Cómo funciona el alias en Unix?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (181, 'How can you redirect I/O in Unix?', '¿Cómo se pueden redirigir E/S en Unix?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (182, 'What are the main steps taken by a Unix Shell for processing a command?', '¿Cuáles son los principales pasos que sigue un Unix Shell para procesar un comando?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (183, 'What is a Sticky bit in Unix?', '¿Qué es un bit adhesivo en Unix?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (184, 'What are the different outputs from Kill command in Unix?', '¿Cuáles son los diferentes resultados del comando Kill en Unix?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (185, 'How will you customize your environment in Unix?', '¿Cómo personalizarás tu entorno en Unix?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (186, 'What are the popular commands for user management in Unix?', '¿Cuáles son los comandos populares para la gestión de usuarios en Unix?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (187, 'How will you debug a shell script in Unix?', '¿Cómo se depurará un script de shell en Unix?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (188, 'What is the difference between a Zombie and Orphan process in Unix?', '¿Cuál es la diferencia entre un proceso Zombie y Orphan en Unix?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (189, 'How will you check if a remote host is still alive?', '¿Cómo comprobará si un host remoto todavía está vivo?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (190, 'How will you get the last executed command in Unix?', '¿Cómo obtendrás el último comando ejecutado en Unix?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (191, 'What is the meaning of “2>&1” in a Unix shell?', '¿Cuál es el significado de “2>&1” en un shell Unix?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (192, 'How will you find which process is taking most CPU time in Unix?', '¿Cómo encontrará qué proceso consume más tiempo de CPU en Unix?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (193, 'What is the difference between Soft link and Hardlink in Unix?', '¿Cuál es la diferencia entre enlace suave y enlace duro en Unix?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (194, 'How will you find which processes are using a file?', '¿Cómo encontrará qué procesos están utilizando un archivo?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (195, 'What is the purpose of nohup in Unix?', '¿Cuál es el propósito de nohup en Unix?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (196, 'How will you remove blank lines from a file in Unix?', '¿Cómo se eliminan las líneas en blanco de un archivo en Unix?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (197, 'How will you find the remote hosts that are connecting to your system on a specific port in Unix?', '¿Cómo encontrará los hosts remotos que se conectan a su sistema en un puerto específico en Unix?', 'Unix Questions ');
INSERT INTO `questions` (`id`, `question`, `pregunta`, `category`) VALUES (198, 'What is xargs in Unix?', '¿Qué es xargs en Unix?', 'Unix Questions ');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
